# DKX/Angular/Auth

Authorization module for angular.

## Installation

```bash
$ npm install --save @dkx/ng-auth
```

or with yarn

```bash
$ yarn add @dkx/ng-auth
```

## Configure 

```typescript
import {NgModule, Injectable} from '@angular/core';
import {AuthModule, AbstractAuthConfiguration} from '@dkx/ng-auth';

@Injectable()
class AuthConfiguration extends AbstractAuthConfiguration
{
    
    public getStoredToken(): string|undefined
    {
        return loadStoredAuthTokenSomehow();
    }
    
}

@NgModule({
    imports: [
        AuthModule.forRoot(AuthConfiguration),
    ],
    exports: [
        AuthModule,
    ],
})
export class AppModule {}
```

Since this library is token agnostic, you must provide also the token decoder service. You can write your own or use one
of these:

* [@dkx/ng-auth-jwt](https://gitlab.com/dkx/angular/auth-jwt)

## AuthService

`AuthService` can be used for accessing and working with current authorization token.

* `token`: Getter for current token. It does not contain any validation.
* `decodeToken()`: Returns decoded token.
* `validateToken()`: Check if token is valid.
* `getTokenExpirationDate()`: Returns instance of `Date` object with expiration date.
* `isTokenExpired()`: Check is token has expired.

## Custom token decoder

```typescript
import {Injectable} from '@angular/core';
import {TokenDecoder} from '@dkx/ng-auth';

export declare interface CustomToken
{
    expirationDate: number,
}

@Injectable()
export class CustomTokenDecoder implements TokenDecoder<CustomToken>
{

    public decodeToken(token: string): CustomToken
    {
        return decodeTokenSomehow(token);
    }

    public extractExpirationDate(tokenData: CustomToken): Date|undefined
    {
        return new Date(tokenData.expirationDate * 1000);
    }

}
```

Now you can register it in your `AppModule`.

```typescript
import {NgModule} from '@angular/core';
import {TOKEN_DECODER} from '@dkx/ng-auth';
import {CustomTokenDecoder} from './custom-token-decoder.service';

@NgModule({
    providers: [
        {
            provide: TOKEN_DECODER,
            useClass: CustomTokenDecoder,
        },
    ],
})
export class AppModule {}
```
