import {HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';


export abstract class AbstractAuthConfiguration
{


	public withCredentials: boolean = false;


	public abstract getStoredToken(): string|undefined;


	public storeToken(token: string): void
	{
	}


	public extractTokenFromHttpResponse<T>(res: HttpResponse<T>): string|undefined
	{
		return undefined;
	}


	public checkHttpResponseError(err: HttpErrorResponse): void
	{
	}


	public appendTokenHttpHeader(headers: HttpHeaders, token: string): HttpHeaders
	{
		return headers.set('Authorization', `Bearer ${token}`);
	}

}
