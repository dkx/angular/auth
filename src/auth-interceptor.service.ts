import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError as ObservableThrow} from 'rxjs';
import {tap, catchError} from 'rxjs/operators';

import {AuthService} from './auth.service';
import {AbstractAuthConfiguration} from './abstract-auth-configuration';


@Injectable()
export class AuthInterceptorService<U, T> implements HttpInterceptor
{


	constructor(
		private $auth: AuthService<U, T>,
		private $config: AbstractAuthConfiguration,
	) {}


	public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
	{
		let duplicate = req.clone({
			withCredentials: this.$config.withCredentials,
		});

		const token = this.$config.getStoredToken();

		if (typeof token !== 'undefined' && this.$auth.validateToken(token)) {
			duplicate = duplicate.clone({
				headers: this.$config.appendTokenHttpHeader(duplicate.headers, token),
			});
		}

		return next.handle(duplicate).pipe(
			tap((event: HttpEvent<any>) => this.processHttpResponse(event)),
			catchError((err: HttpErrorResponse) => this.processHttpResponseError(err)),
		);
	}


	private processHttpResponse(event: HttpEvent<any>): void
	{
		if (event instanceof HttpResponse) {
			const token = this.$config.extractTokenFromHttpResponse(event);

			if (typeof token !== 'undefined') {
				this.$config.storeToken(token);
			}
		}
	}


	private processHttpResponseError(err: HttpErrorResponse): Observable<never>
	{
		this.$config.checkHttpResponseError(err);
		return ObservableThrow(err);
	}

}
