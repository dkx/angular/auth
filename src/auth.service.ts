import {Injectable, Inject} from '@angular/core';

import {AbstractAuthConfiguration} from './abstract-auth-configuration';
import {TokenDecoder, TOKEN_DECODER} from './token-decoder';


@Injectable({
	providedIn: 'root',
})
export class AuthService<U, T>
{


	constructor(
		private $config: AbstractAuthConfiguration,
		@Inject(TOKEN_DECODER) private $tokenDecoder: TokenDecoder<T>,
	) {}


	get token(): string|undefined
	{
		return this.$config.getStoredToken();
	}


	public decodeToken(token: string = this.token): T|undefined
	{
		if (typeof token === 'undefined') {
			return;
		}

		return this.$tokenDecoder.decodeToken(token);
	}


	public validateToken(token: string = this.token): boolean
	{
		return !this.isTokenExpired(token);
	}


	public getTokenExpirationDate(token: string = this.token): Date|undefined
	{
		if (typeof token === 'undefined') {
			return;
		}

		const tokenData = this.decodeToken(token);
		return this.$tokenDecoder.extractExpirationDate(tokenData);
	}


	public isTokenExpired(token: string = this.token): boolean
	{
		const expirationDate = this.getTokenExpirationDate(token);

		if (typeof expirationDate === 'undefined') {
			return true;
		}

		const currentDate = new Date;
		return currentDate.valueOf() > expirationDate.valueOf();
	}

}
