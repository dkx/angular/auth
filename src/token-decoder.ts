import {InjectionToken} from '@angular/core';


export const TOKEN_DECODER = new InjectionToken('TOKEN_DECODER');


export interface TokenDecoder<T>
{


	decodeToken(token: string): T;

	extractExpirationDate(tokenData: T): Date|undefined;

}
