import {NgModule, ModuleWithProviders, Type} from '@angular/core';

import {AbstractAuthConfiguration} from './abstract-auth-configuration';
import {AuthInterceptorService} from './auth-interceptor.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';


@NgModule()
export class AuthModule
{


	public static forRoot(config: Type<AbstractAuthConfiguration>): ModuleWithProviders
	{
		return {
			ngModule: AuthModule,
			providers: [
				{
					provide: AbstractAuthConfiguration,
					useClass: config,
				},
				{
					provide: HTTP_INTERCEPTORS,
					useClass: AuthInterceptorService,
					multi: true,
				},
			],
		};
	}

}
